package meridianid.farizdotid.belajarrxjava;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import meridianid.farizdotid.belajarrxjava.model.GithubResult;
import meridianid.farizdotid.belajarrxjava.server.GithubService;
import meridianid.farizdotid.belajarrxjava.server.UtilsApi;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();

    private Button button_githubservice;
    private TextView txt_resultbio;
    private ProgressDialog loading;
    GithubService mService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mService = UtilsApi.getAPIService();

        initComponents();
    }

    private void initComponents(){
        button_githubservice = (Button) findViewById(R.id.button_githubservice);
        button_githubservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loading = ProgressDialog.show(MainActivity.this, null, "loading...", true, false);
                callGithubService();
            }
        });

        txt_resultbio = (TextView) findViewById(R.id.txt_resultbio);
    }

    private void callGithubService(){
        Observable<GithubResult> githubUser = mService.getGithubUser("farizdotid");
        githubUser.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GithubResult>() {
                    @Override
                    public void onCompleted() {
                        Log.d(TAG, "onCompleted: ");
                        loading.dismiss();
                        Toasty.success(MainActivity.this, "berhasil", Toast.LENGTH_SHORT, true)
                                .show();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: " + e.getMessage());
                        loading.dismiss();
                        Toasty.error(MainActivity.this, e.getMessage(), Toast.LENGTH_SHORT, true)
                                .show();
                    }

                    @Override
                    public void onNext(GithubResult githubResult) {
                        Log.i(TAG, "onNext: BIO > " + githubResult.getBio());
                        txt_resultbio.setText(githubResult.getBio().toString());
                        Log.i(TAG, "onNext: init textview");
                    }
                });
    }
}
