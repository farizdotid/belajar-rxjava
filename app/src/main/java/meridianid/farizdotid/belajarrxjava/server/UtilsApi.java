package meridianid.farizdotid.belajarrxjava.server;

/**
 * Created by farizdotid on 2/28/2017.
 */

public class UtilsApi {

    public static final String BASE_URL = "https://api.github.com/";

    public static GithubService getAPIService(){
        return RetrofitClient.getClient(BASE_URL).create(GithubService.class);
    }
}
