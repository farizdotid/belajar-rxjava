package meridianid.farizdotid.belajarrxjava.server;

import meridianid.farizdotid.belajarrxjava.model.GithubResult;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by farizdotid on 2/28/2017.
 */

public interface GithubService {

    @GET("users/{username}")
    rx.Observable<GithubResult> getGithubUser(@Path("username") String username);
}
